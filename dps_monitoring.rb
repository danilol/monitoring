#!/usr/bin/ruby
require "open-uri"

SEPARATOR = '|'
SERVICES = [ 'Account Management', 'Analytics', 'App Builder', 'Production: Upload',
             'Production: Publishing', 'Push Notification', 'Social Content Processing',
             'Content Distribution', 'Content Viewer for Web' ]

body = open('http://status.adobedps.com').read

def color_to_status(color)
  case color
  when "g"
    "OK"
  when "r"
    "FAIL"
  when "y"
    "WARNING"
  else
    "UNKNOWN"
  end
end

SERVICES.each do |service|
  found = body =~ /"([rgy])", "#{service}"/
  status = found ? color_to_status($1) : 'HTML broken, sercvice status not found'
  puts [service, status].join(SEPARATOR)
end
