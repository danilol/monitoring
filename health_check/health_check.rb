# -*- encoding : utf-8 -*-
Dir['./apps/*'].each{ |f| require f }
require 'faraday_ext'
require 'faraday_middleware'
require 'colorize'
require 'benchmark'

class HealthCheck
  class << self
    ::FILEPATH       = './'
    ::FILENAME       = 'result.txt'
    ::RETURN_FAILED  = 'RED'
    ::RETURN_SUCCESS = 'GREEN'

    ::NORMAL   = "NORMAL"
    ::WARNING  = "WARNING"
    ::CRITICAL = "CRITICAL"

    #  Ex.: ARGV 1 2 3 4 suppress quiet
    #  Input parameters (separated by spaces):
    #    1 2 3 4: Checking index. If not given, all the checks will be executed.
    #    quiet: Runs the script silently without any return on the screen, only generates the file. If not given, the script will print on the screen the progress information and the check statuses.
    #    suppress: Suppresses in the output file, the services with normal status, returning only those with abnormal statuses. If not given, all services will be returned in the file.
    def run_checks
      setup
      header

      output = []

      output << DLS.run_check              if ARGV.empty? or ARGV.include?("1")
      output << ESS.run_check              if ARGV.empty? or ARGV.include?("2")
      output << AVS.run_check              if ARGV.empty? or ARGV.include?("3")
      output << Orders.run_check           if ARGV.empty? or ARGV.include?("4")
      output << Subscriptions.run_check    if ARGV.empty? or ARGV.include?("5")
      output << Profiles.run_check         if ARGV.empty? or ARGV.include?("6")
      output << Commerce.run_check         if ARGV.empty? or ARGV.include?("7")
      output << Catalogue.run_check        if ARGV.empty? or ARGV.include?("8")
      output << Entitlements.run_check     if ARGV.empty? or ARGV.include?("9")
      Store.run_check                      if ARGV.empty? or ARGV.include?("10")
      output << DPS.run_check              if ARGV.empty? or ARGV.include?("11")
      output << AES.run_check              if ARGV.empty? or ARGV.include?("12")
      Jobs.run_check                       if ARGV.empty? or ARGV.include?("13")
      output << ContentServer.run_check    if ARGV.empty? or ARGV.include?("14")
      output << ExternalContent.run_check  if ARGV.empty? or ARGV.include?("15")
      output << Assine.run_check           if ARGV.empty? or ARGV.include?("16")
      output << Search.run_check           if ARGV.empty? or ARGV.include?("17")
      output << GlobalCollect.run_check    if ARGV.empty? or ARGV.include?("18")
      output << Invoices.run_check         if ARGV.empty? or ARGV.include?("19")
      output << PartnersHotsite.run_check  if ARGV.empty? or ARGV.include?("20")
      output << AbrilEducacao.run_check    if ARGV.empty? or ARGV.include?("21")
      output << Admin.run_check            if ARGV.empty? or ARGV.include?("22")
      output << Callcenter.run_check       if ARGV.empty? or ARGV.include?("23")
      output << Payments.run_check         if ARGV.empty? or ARGV.include?("24")
      output << ContentManager.run_check   if ARGV.empty? or ARGV.include?("25")
      output << ReaderSupport.run_check    if ARGV.empty? or ARGV.include?("26")
      output << Distributors.run_check     if ARGV.empty? or ARGV.include?("27")

      write_file(output.flatten)
    end

    def print_success(spaces = 2)
      print_green "SUCCESS".rjust(7 + spaces) unless @quiet
    end

    def print_failed(spaces = 2)
      print_red "FAILED".rjust(6 + spaces) unless @quiet
    end

    def print_yellow(txt)
      puts txt.yellow unless @quiet
    end

    def print_red(txt)
      puts txt.red
    end

    def print_green(txt)
      puts txt.green
    end

    def check_situation(time, min, max)
      return WARNING  if time.between?(min, max)
      return CRITICAL if time > max
      NORMAL
    end

    def situation_ok?(status)
      status != CRITICAL
    end

    def header
      return if @quiet

      puts ""
      print_yellow "  ##########################################################################"
      print_yellow "##                                                                          ##"
      print_yellow "##   Monitoramento das Aplicações                                           ##"
      print_yellow "##                                                                          ##"
      print_yellow "##   https://confluence.abril.com.br/pages/viewpage.action?pageId=33293057  ##"
      print_yellow "##                                                                          ##"
      print_yellow "  ##########################################################################"
      puts ""
    end

    def setup
      @quiet = ARGV.include?("quiet")
      ARGV.delete("quiet") if @quiet

      @suppress = ARGV.include?("suppress")
      ARGV.delete("suppress") if @suppress
    end

    def quiet?
      @quiet
    end

    def must_suppress?(situation)
      @suppress and situation == NORMAL
    end

    def write_file(lines)
      lines.compact!
      lines.insert(0, 'APP|SERVICE|HEALTH|TIME_RESPONSE')
      File.open(FILEPATH + FILENAME, "w+") { |f| lines.each { |line| f.write("#{line}\n") } }
      puts "Total de registros: #{lines.count - 1} - Exported to #{FILENAME}" unless @quiet
    end
  end
end

HealthCheck.run_checks
