# -*- encoding : utf-8 -*-
class AVS
  AVS_URL = 'https://api-avs.abrildigital.com.br/AdobeAuth'

  def self.run_check
    output = []

    HealthCheck::print_yellow "3. Adobe Vendor Support (AVS) - API responsável por autenticar o usuário na Adobe"
    HealthCheck::print_yellow "  3.1. Solicitar ao DLS Token de Ativação do Reader de Livros Verificando se o mesmo é Retornado - Serviço de autenticação de usuário do reader na Adobe"
    output << check_sign_in

    HealthCheck::print_yellow "  3.2 - Health-Check do AVS - Verifica todos os serviços necessários para manter a aplicação UP"
    output << health_check
  end

  private
  def self.check_sign_in
    description = "AVS|SignIn"

    token = Base64.encode64(DLS.get_adobe_auth_token).chomp

    time = Benchmark.measure { @response = sign_in(token) }

    situation = HealthCheck.check_situation(time.real, 5, 10)

    if @response.status == 200 and @response.body["signInResponse"]["label"] and @response.body["signInResponse"]["user"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.health_check
    description = "AVS|HealthCheck"

    time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{AVS_URL}/Status").get }

    situation = HealthCheck.check_situation(time.real, 5, 10)

    if @response.status == 200 and @response.body == "UP" and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.sign_in(token)
    FaradayExt::Utils::ConnectionBuilder.standard("#{AVS_URL}/SignIn").post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/vnd.adobe.adept+xml'})
      request.body = "<signInRequest method=\"standard\" xmlns=\"http://ns.adobe.com/adept\"><authData>#{token}</authData></signInRequest>"
    end
  end
end
