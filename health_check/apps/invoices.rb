# -*- encoding : utf-8 -*-
class Invoices
  def self.run_check
    HealthCheck::print_yellow "19. Backend: invoices - API Responsável por faturar os pedidos do iba"
    HealthCheck::print_yellow "  19.1 Checar todos os serviços necessários para manter a aplicação UP"
    health_check
  end

  private
  def self.health_check
    description = "INVOICES|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://invoices.iba.com.br/health_check').get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
