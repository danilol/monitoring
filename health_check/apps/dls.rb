# -*- encoding : utf-8 -*-
class DLS
  DLS_URL = 'https://api-dls.iba.com.br/iba'

  def self.run_check
    output = []

    HealthCheck::print_yellow "1. Digital Library Services ( DLS ) - API responsável por gerenciar a entrega de conteúdo para a biblioteca do usuário nos Readers de Jornais e Livros"
    HealthCheck::print_yellow "  1.1 Recuperar Metadados de Conteúdo (Livros e Jornais)"
    HealthCheck::print_yellow "    1.1.1 - Livros - GetSingleItems"
    output << check_get_single_items_book

    HealthCheck::print_yellow "    1.1.2 - Jornais - GetSingleItems"
    output << check_get_single_items_newspaper

    HealthCheck::print_yellow "    1.1.3 - Jornais - GetSubscriptionsItems"
    output << check_get_subscription_items_newspaper
  end

  private
  def self.check_get_single_items_book
    description = "DLS|GetSingleItemsBook"

    time = Benchmark.measure { @response = get_single_items('book') }

    situation = HealthCheck.check_situation(time.real, 10, 15)

    if @response.status == 200 and @response.body["result"]["totalEntries"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(4)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(4)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_get_single_items_newspaper
    description = "DLS|GetSingleItemsNewspaper"

    time = Benchmark.measure { @response = get_single_items('newspaper') }

    situation = HealthCheck.check_situation(time.real, 10, 15)

    if @response.status == 200 and @response.body["result"]["totalEntries"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(4)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(4)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_get_subscription_items_newspaper
    description = "DLS|GetSubscriptionItemsNewspaper"

    time = Benchmark.measure { @response = get_subscription_items }

    situation = HealthCheck.check_situation(time.real, 20, 25)

    if @response.status == 200 and @response.body["result"]["totalEntries"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(4)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(4)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.get_single_items(type)
    FaradayExt::Utils::ConnectionBuilder.standard(DLS_URL).post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/json'})
      request.body = get_single_items_body(type)
    end
  end

  def self.get_single_items_body(type)
    {
      'jsonrpc' => "2.0",
      'method' => "getSingleItems",
      'params' =>  { "readerName" => "iba ebooks iPad", "readerVersion" => "3.0",
        "manufacturer" =>  "Dell Inc.", "model" => "Vostro 1320", "deviceId" => "monitoração",
        "perPage" => "1000", "productType" => type,
        "auth" => { "email" => "testedigitaltablet1@gmail.com", "password" => "ipad1234" }
    }
    }.to_json
  end

  def self.get_subscription_items
    FaradayExt::Utils::ConnectionBuilder.standard(DLS_URL).post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/json'})
      request.body = get_subscription_items_body
    end
  end

  def self.get_subscription_items_body
    {
      'jsonrpc' => "2.0",
      'method' => "getSubscriptionItems",
      'params' =>  { "readerName" => "iba ebooks iPad", "readerVersion" => "3.0",
        "manufacturer" =>  "Dell Inc.", "model" => "Vostro 1320", "deviceId" => "monitoração",
        "perPage" => "1000", "productType" => "newspaper",
        "auth" => { "email" => "testedigitaltablet1@gmail.com", "password" => "ipad1234" }
      }
    }.to_json
  end

  def self.get_adobe_auth_token
    response = FaradayExt::Utils::ConnectionBuilder.standard(DLS_URL).post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/json'})
      request.body = get_adobe_auth_token_body
    end

    response.body["result"]["token"] if response.status == 200
  end

  def self.get_adobe_auth_token_body
    {
      'jsonrpc' => "2.0",
      'method' => "getAdobeAuthToken",
      'params' =>  { "readerName" => "iba ebooks iPad", "readerVersion" => "3.0",
        "manufacturer" =>  "Dell Inc.", "model" => "Vostro 1320", "deviceId" => "monitoração",
        "auth" => { "email" => "testedigitaltablet1@gmail.com", "password" => "ipad1234" }
      }
    }.to_json
  end
end
