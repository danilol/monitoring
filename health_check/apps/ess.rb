# -*- encoding : utf-8 -*-
class ESS
  def self.run_check
    HealthCheck::print_yellow "2. Ecommerce Support Services(ESS) - API responsável por autenticações de usuários em sistemas legados e comunicação com cadastro corporativo da Abril (CRP)"
    HealthCheck::print_yellow "  2.1 - Simulação de Comportamento como Proxy para Autenticação"
    authenticate
  end

  private
  def self.authenticate
    description = "ESS|Authenticate"

    time = Benchmark.measure do
      @response = FaradayExt::Utils::ConnectionBuilder.standard('https://ess2.iba.com.br/usuarios/autenticar', { timeout: 60, digest_auth: { user: "DLS", password: "26DZjfX6ZbsCn8QVBsvRZABIE0B5nzLNT2cc8JGyFl9Vh7kt"}}) .post do |request|
        request.headers.merge!({ 'Content-Type' => 'application/x-www-form-urlencoded'})
        request.body = 'email=testedigitaltablet1@gmail.com&senha=ipad1234'
      end
    end

    situation = HealthCheck.check_situation(time.real, 5, 10)

    if @response.status == 200 and @response.body["data"]["href_usuario"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
