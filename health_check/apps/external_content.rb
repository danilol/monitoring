# -*- encoding : utf-8 -*-
class ExternalContent
  def self.run_check
    output = []

    HealthCheck::print_yellow "15.1 Monitorar o Adobe Content Server de distribuidores"
    HealthCheck::print_yellow "  15.1.1 Leya"
    output << check_leya

    HealthCheck::print_yellow "  15.1.2 Xeriph"
    output << check_xeriph

    HealthCheck::print_yellow "  15.1.3 DLD"
    output << check_dld
  end

  private
  def self.check_leya
    description = "LEYA|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://acs.leya.com:8080/fulfillment/Status').get }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.body == "UP" and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_xeriph
    description = "XERIPH|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://drm.xeriph.com.br:8080/fulfillment/Status').get }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.body == "UP" and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_dld
    description = "DLD|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://acs.plataformadld.com.br/fulfillment/Status').get }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.body == "UP" and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
