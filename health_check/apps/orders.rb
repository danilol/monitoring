# -*- encoding : utf-8 -*-
class Orders
  def self.run_check
    HealthCheck::print_yellow "4. Backend: Orders - API responsável pelos pedidos do iba"
    HealthCheck::print_yellow "  4.1 - Realizar Recuperação de Pedidos Pendentes - Checa todos os serviços necessários para manter a aplicação UP"
    health_check
  end

  private
  def self.health_check
    description = "ORDERS|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://orders.iba.com.br/health_check').get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
