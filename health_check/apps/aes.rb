# -*- encoding : utf-8 -*-
class AES
  AES_URL                 = 'http://aes.iba.com.br'
  PRODUCT_IDS             = ["br.com.abril.iba.boaforma.dietas", "br.com.abril.iba.sexy.melho", "br.com.abril.revnovaescolanotablet0260", "br.com.caras.revcaras.1001", "br.com.abril.revexamenoipad0986"]
  THIRD_PARTY_PRODUCT_IDS = ["br.com.abril.revvejanoipad2196", "br.com.abril.revvejanoipad2197", "br.com.abril.revvejanoipad2198", "br.com.abril.revvejanoipad2199", "br.com.abril.revvejanoipad2200"]
  AE_PRODUCT_IDS          = ["br.com.atica.kiosk.v9788508166350_a_1", "br.com.atica.kiosk.vp9788508166398_a_1", "br.com.atica.kiosk.vb9788526290266_a_1", "br.com.atica.kiosk.vh9788508166374_a_1"]

  def self.run_check
    output = []
    HealthCheck::print_yellow("12. Adobe Entitlement Server - Sistema responsável integrar a as APIs do IBA com os Readers.")
    HealthCheck::print_yellow("  12.1 Simular Requisição de Usuário na Busca de Metadados")
    HealthCheck::print_yellow("    12.1.1 -Itens Avulsos - IBA")
    HealthCheck::print_yellow("      12.1.1.1 - Autenticação e retorno do token - Faz a autenticação do usuário e retorna o token para ser utilizado nas outras requisições.")
    output << check_sign_in_with_credentials

    HealthCheck::print_yellow("      12.1.1.2 - Buscar os entitlements (itens avulsos e itens de assinatura que o usuário tem direito)")
    output << check_entitlements

    HealthCheck::print_yellow("      12.1.1.3 - Verificar se usuário tem direito ao entitlement")
    output << check_verify_entitlements

    HealthCheck::print_yellow("    12.1.2 -Itens de Assinatura - ThirdParty (Aplicativo da Marca) - Os aplicativos de marca fazem esta chamada para buscar os items que o usuário tem direito.")
    HealthCheck::print_yellow("      12.1.2.1 - Autenticação e retorno do token - Faz a autenticação do usuário no Assine e retorna o token para ser utilizado nas outras requisições.")
    output << check_sign_in_with_credentials_third_party

    HealthCheck::print_yellow("      12.1.2.2 - Verificar se o token do usuário é renovado com sucesso")
    output << check_renew_auth_token_third_party

    HealthCheck::print_yellow("      12.1.2.3 - Buscar os entitlements (items de assinatura que o usuário tem direito)")
    output << check_entitlements_third_party

    HealthCheck::print_yellow("      12.1.2.4 - Verificar se o usuário tem direito ao item de assinatura")
    output << check_verify_entitlements_third_party

    HealthCheck::print_yellow("    12.1.3 - Items Abril Educação - APIS ")
    HealthCheck::print_yellow("      12.1.3.1 - Autenticação e retorno do token - Faz a autenticação do usuário e retorna o token para ser utilizado nas outras requisições.")
    output << check_sign_in_with_credentials_abril_educacao

    HealthCheck::print_yellow("      12.1.3.2 - Buscar os entitlements (items no formato \"folio\" que o usuário abril educação tem direito)")
    output << check_entitlements_abril_educacao

    HealthCheck::print_yellow("      12.1.3.3 - Verificar se o usuário tem direito ao item da abril educação")
    output << check_verify_entitlements_abril_educacao
  end

  private

  def self.check_sign_in_with_credentials
    description = "AES|SignInWithCredentials"

    begin
      time = Benchmark.measure { @response = sign_in_with_credentials("SignInWithCredentials", "uuid=1230&appId=br.com.iba.magazinesdesktop&appVersion=2.2.0&platform=iPad") }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["authToken"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_entitlements
    description = "AES|Entitlements"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/entitlements?authToken=ff40119e42e26111dd352d0f5d3cf0b942e0e7d36426a30557e1e9b17cecb1a9&appId=br.com.iba.magazinesdesktop&appVersion=2.2.0").post }

      situation = HealthCheck.check_situation(time.real, 20, 30)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["entitlements"]['productId'].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_verify_entitlements
    description = "AES|VerifyEntitlements"

    begin
      time = Benchmark.measure { @response = verify_entitlements("verifyEntitlement", "authToken=ff40119e42e26111dd352d0f5d3cf0b942e0e7d36426a30557e1e9b17cecb1a9&uuid=cbbffeb8ebd28a05499ac193b7b9c50b09cd885229381f52c89ca286fd64d3a6&appId=br.com.iba.magazinesdesktop&appVersion=2.2.0", PRODUCT_IDS) }

      situation = HealthCheck.check_situation(time.real, 20, 30)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response == true and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_sign_in_with_credentials_third_party
    description = "AES|SignInWithCredentialsThirdParty"

    begin
      time = Benchmark.measure { @response = sign_in_with_credentials("third_party/SignInWithCredentials", "uuid=1230&appId=br.com.abril.revvejanoipad&appVersion=2.2.0&platform=iPad") }

      situation = HealthCheck.check_situation(time.real, 15, 20)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["authToken"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_renew_auth_token_third_party
    description = "AES|RenewAuthTokenThirdParty"

    begin
      @response = sign_in_with_credentials("third_party/SignInWithCredentials", "uuid=1230&appId=br.com.abril.revvejanoipad&appVersion=2.2.0&platform=iPad")
      old_token = @response.body["result"]["authToken"]

      time = Benchmark.measure { @response = renew_auth_token_third_party(old_token) }

      situation = HealthCheck.check_situation(time.real, 15, 20)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["authToken"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_entitlements_third_party
    description = "AES|EntitlementsThirdParty"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/third_party/entitlements?authToken=e3f35dd1773f3077415158790854513240a764e575b1ee1759d487b57e1fa126&appId=br.com.abril.revvejanoipad&appVersion=2.2.0").post }

      situation = HealthCheck.check_situation(time.real, 20, 30)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["entitlements"]['productId'].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_verify_entitlements_third_party
    description = "AES|VerifyEntitlementsThirdParty"

    begin
      @response = sign_in_with_credentials("third_party/SignInWithCredentials", "uuid=1230&appId=br.com.abril.revvejanoipad&appVersion=2.2.0&platform=iPad")
      token = @response.body["result"]["authToken"]

      time = Benchmark.measure { @response = verify_entitlements("third_party/verifyEntitlement", "authToken=#{token}&uuid=1230&appId=br.com.abril.revvejanoipad&appVersion=2.2.0&platform=iPad", THIRD_PARTY_PRODUCT_IDS) }

      situation = HealthCheck.check_situation(time.real, 20, 30)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response == true and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.sign_in_with_credentials(uri, params)
    FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/#{uri}?#{params}").post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/xml'})
      request.body = '<?xml version="1.0" encoding="UTF-8"?><credentials><emailAddress>testedigitaltablet1@gmail.com</emailAddress><password>ipad1234</password></credentials>'
    end
  end

  def self.verify_entitlements(uri, params, products)
    up = false

    products.each do |product|
      response = FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/#{uri}?#{params}&productId=#{product}").get

      up = true if response.status == 200 and response.body["result"]["entitled"] == "true"
    end

    up
  end

  def self.renew_auth_token_third_party(token)
    FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/third_party/RenewAuthToken?authToken=#{token}&uuid=1230&appId=br.com.abril.revvejanoipad&appVersion=2.2.0&platform=iPad").get
  end

  def self.check_sign_in_with_credentials_abril_educacao
    description = "AES|SignInWithCredentialsAbrilEducacao"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/#{"abrileducacao/SignInWithCredentials"}?emailAddress=testedigitaltablet1@gmail.com&password=ipad1234&uuid=2DB7AB57-52B6-4AB7-A389-DE71F9557B2A&appId=br.com.atica.kiosk&appVersion=1.0.13").get }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["authToken"] and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_entitlements_abril_educacao
    description = "AES|EntitlementsThirdParty"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{AES_URL}/abrileducacao/entitlements?authToken=d9812a37042263e89ac732852f959ece38dfea404dec0ea4a9d6dc23eb297c07&uuid=2DB7AB57-52B6-4AB7-A389-DE71F9557B2A&appId=br.com.atica.kiosk&appVersion=1.0.13").get }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["result"]["entitlements"]['productId'].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_verify_entitlements_abril_educacao
    description = "AES|VerifyEntitlementsAbrilEducacao"

    begin
      time = Benchmark.measure { @response = verify_entitlements("abrileducacao/verifyEntitlement", "authToken=d9812a37042263e89ac732852f959ece38dfea404dec0ea4a9d6dc23eb297c07&uuid=0&appId=br.com.atica.kiosk&appVersion=1.0.13&platform=iPad&productId=", AE_PRODUCT_IDS) }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response == true and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(6)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(6)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
