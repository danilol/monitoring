# -*- encoding : utf-8 -*-
class DPS
  DPS_URL = "http://edge.adobe-dcfs.com/ddp/issueServer/issues"

  def self.run_check
    output = []

    HealthCheck::print_yellow "11. Digital Publishing Suite"
    HealthCheck::print_yellow "  11.1 - (Solicitar Conteúdo Específico de Usuário) - Solicitar metadados de issues da conta agregada"
    HealthCheck::print_yellow "    11.1.1 - Desktop"
    output << desktop_health_check

    HealthCheck::print_yellow "    11.1.2 - iPad"
    output << ipad_health_check

    HealthCheck::print_yellow "    11.1.3 - android"
    output << android_health_check
  end

  private
  def self.desktop_health_check
    description = "DPS|DesktopHealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{DPS_URL}?accountId=bf8b5e4490f041e6a25428b1d175b7b9&targetDimension=all").get }

      situation = HealthCheck.check_situation(time.real, 5, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body['results']['issues']['issue'].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(4)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(4)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.ipad_health_check
    description = "DPS|IpadHealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{DPS_URL}?accountId=09c3b3b6cf3349a2951e0dad11951ab5&targetDimension=1024x768,768x1024,1024x748,748x1024").get }

      situation = HealthCheck.check_situation(time.real, 5, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body['results']['issues']['issue'].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(4)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(4)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.android_health_check
    description = "DPS|AndroidHealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("#{DPS_URL}?accountId=ec14a233a05840179aac9820e859a460&targetDimension=all").get }

      situation = HealthCheck.check_situation(time.real, 5, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body['results']['issues']['issue'].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success(4)
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed(4)
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
