# -*- encoding : utf-8 -*-
class Assine

  def self.run_check
    output = []
    HealthCheck::print_yellow "16. Assineabril.com - Sistema responsável por gerenciamento de assinantes da Abril"
    HealthCheck::print_yellow "  16.1 Consulta de assinante direto do AssineAbril"
    output << check_consultar_assinante

    HealthCheck::print_yellow "  16.2 Testar a Autenticação do Usuário através do Assine (ao Invés de Utilizar Usuário do iba)"
    output << check_autenticar_assinante

    HealthCheck::print_yellow "  16.3 Consultar health check - Checa o status do server do iframe de Assinatura"
    #output << health_check
  end

  private
  def self.check_consultar_assinante
    description = "ASSINE|CONSULTARASSINANTE"

    begin
      time = Benchmark.measure { @response = consultar_assinante }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["codigo"] == 1 and @response.body["mensagem"] == "Operacao consultarAssinantes realizada com sucesso." and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.check_autenticar_assinante
    description = "ASSINE|AUTENTICAR"

    begin
      time = Benchmark.measure { @response = autenticar_assinante }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["codigo"] == 1 and @response.body["mensagem"] == "Operacao autenticarUsuario realizada com sucesso." and @response.body["novosAssinantesTO"].first["projetos"].count > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.health_check
    description = "ASSINE|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("https://www.assine.abril.com.br/portal/pagamento").get }

      situation = HealthCheck.check_situation(time.real, 15, 20)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.body.include?("<script>popularAreasSegmentadas(undefined, undefined, 'DQ3T');</script>") and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.consultar_assinante
    FaradayExt::Utils::ConnectionBuilder.standard("http://www.assine.abril.com.br/portal/services/portalAssineRestServices/consultarAssinantes/codigoSistema/7").post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/json'})
      request.headers.merge!({ 'Accept' => 'application/json'})
      request.body = "{\"codigosAssinantes\": [\"527113682\"]}"
    end
  end

  def self.autenticar_assinante
    FaradayExt::Utils::ConnectionBuilder.standard("http://www.assine.abril.com.br/portal/services/portalAssineRestServices/autenticarUsuario/codigoSistema/7").post do |request|
      request.headers.merge!({ 'Content-Type' => 'application/json'})
      request.headers.merge!({ 'Accept' => 'application/json'})
      request.body = "{\"email\":\"testedigitaltablet1@gmail.com\",\"senha\":\"ipad1234\",\"somenteProjDigital\" : \"S\", \"somenteAutentica\" : \"N\"}"
    end
  end
end
