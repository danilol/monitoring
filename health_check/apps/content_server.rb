# -*- encoding : utf-8 -*-
class ContentServer
  def self.run_check
    HealthCheck::print_yellow "14. Adobe Content Server"
    HealthCheck::print_yellow "  14.1 Monitorar o Adobe Content Server"
    health_check
  end

  private
  def self.health_check
    description = "CONTENTSERVER|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://acs4.iba.com.br:8080/fulfillment/Status').get }

      situation = HealthCheck.check_situation(time.real, 10, 15)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.body == "UP" and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
