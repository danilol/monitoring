# -*- encoding : utf-8 -*-
class Admin
  def self.run_check
    HealthCheck::print_yellow "22 - Admin - Gerenciamento de conteúdos, campanhas, operacional do iba"
    HealthCheck::print_yellow "  22.1 - Consultar Health Check - Checar todos os serviços necessários para manter a aplicação UP"
    health_check
  end

  private
  def self.health_check
    description = "ADMIN|HealthCheck"
    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('https://admin.iba.com.br/health_check').get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
