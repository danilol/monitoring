# -*- encoding : utf-8 -*-
class Distributors
  def self.run_check
    output = []
    HealthCheck::print_yellow "27 - Distribuidoras - Parceiros que vendem livros de várias de editoras através de integração com o iba"
    HealthCheck::print_yellow "27.1 - DLD - Health Check"
    output << dld_health_check

    HealthCheck::print_yellow "27.2 - OVERDRIVE - Health Check"
    #output << overdrive_health_check

    HealthCheck::print_yellow "27.3 - XERIPH - Health Check"
    output << xeriph_health_check

    HealthCheck::print_yellow "27.4 - LEYA - Health Check"
    #output << leya_health_check
  end

  private
  def self.dld_health_check
    description = "DLD|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard("http://api.plataformadld.com.br/api/server_datetime").get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.xeriph_health_check
    description = "XERIPH|HealthCheck"

    begin
      time = Benchmark.measure { @response = Faraday.new("http://api.xeriph.com.br/2.0/alive//") { |b| b.use FaradayMiddleware::FollowRedirects; b.adapter :net_http }.get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
