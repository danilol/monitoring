# -*- encoding : utf-8 -*-
class GlobalCollect
  def self.run_check
    HealthCheck::print_yellow "18. Backend: Global Collect - Gateway de Pagamento do iba"
    HealthCheck::print_yellow "  18.1 Consultar health check - Checa o status do server da GC"
    check_search
  end

  private
  def self.check_search
    description = "GLOBALCOLLECT|HealthCheck"

    begin
      time = Benchmark.measure { @response = search }

      situation = HealthCheck.check_situation(time.real, 15, 20)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["XML"]["REQUEST"]["RESPONSE"]["RESULT"] == "OK" and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end

  def self.search
    FaradayExt::Utils::ConnectionBuilder.standard("https://ps.gcsip.com/wdl/wdl").post do |request|
      request.headers.merge!({ 'Content-Type' => 'text/xml;charset=UTF-8'})
      request.body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><XML><REQUEST><ACTION>TEST_CONNECTION</ACTION><META><IPADDRESS/><MERCHANTID>7302</MERCHANTID><VERSION>2.0</VERSION></META><PARAMS></PARAMS></REQUEST></XML>"
    end
  end
end
