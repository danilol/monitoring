# -*- encoding : utf-8 -*-
class Search
  def self.run_check
    HealthCheck::print_yellow "17 Search Loja - API da Abril responsável pelas buscas na loja iba e demais sites da empresa"
    HealthCheck::print_yellow "  17.1 Search da Loja - Monitoração do serviço"
    check_search
  end

  private
  def self.check_search
    description = "ABRILSEARCH|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('https://www.iba.com.br/public/search/v2/iba/documentos').get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and @response.body["total_results"].to_i > 0 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
