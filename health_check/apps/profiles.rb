# -*- encoding : utf-8 -*-
class Profiles
  def self.run_check
    HealthCheck::print_yellow "6. Backend: Profiles - API responsável por gerenciar cadastros/login entre outras funcionalidades dos usuários do iba."
    HealthCheck::print_yellow "  6.1 - Backend: Profiles - API responsável por gerenciar cadastros/login entre outras funcionalidades dos usuários do iba."
    health_check
  end

  private
  def self.health_check
    description = "PROFILES|HealthCheck"

    begin
      time = Benchmark.measure { @response = FaradayExt::Utils::ConnectionBuilder.standard('http://profiles.iba.com.br/health_check').get }

      situation = HealthCheck.check_situation(time.real, 5, 10)
    rescue
      HealthCheck.print_failed
      return "#{description}|#{RETURN_FAILED}|#{CRITICAL}"
    end

    if @response.status == 200 and HealthCheck.situation_ok?(situation)
      HealthCheck.print_success
      "#{description}|#{RETURN_SUCCESS}|#{situation}" unless HealthCheck.must_suppress?(situation)
    else
      HealthCheck.print_failed
      "#{description}|#{RETURN_FAILED}|#{situation}"
    end
  end
end
