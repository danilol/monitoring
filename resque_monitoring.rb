#!/usr/bin/ruby
require 'resque'
require 'active_support/all'

class ResqueMonitoring

  #alarm situation
  CRITICAL, WARNING, NORMAL = "CRITICAL", "WARNING", "NORMAL"

  #job rank
  RANK_A, RANK_B, RANK_C, RANK_D, RANK_E, RANK_F, RANK_G = 'A', 'B', 'C', 'D', 'E', 'F', 'G'

  #job impact
  LOW, MEDIUM, HIGH =  'LOW', 'MEDIUM', 'HIGH'

  #job type
  SINGLE, MASS = 'SINGLE', 'MASS'

  #file options
  FILEPATH = '/tmp/'

  def initialize
    load_resque
    run_checks
  end

  def run_checks
    check_failed
    check_pending
    check_workers
  end

  ######################################################
  ### Failed
  ######################################################
  def check_failed
    lines = []
    unknown_lines = []

    @queues.each do |queue|
      count = 0
      job = @jobs.detect{ |k| k[:key] == queue }

      @failed_jobs.each do |failed_job|
        count += 1 if failed_job["queue"] == queue
      end

      if job
        job[:count] = count
        situation = check_for_failed_situation(job)
      else
        unknown_job = { key: queue, impact: LOW, rank: RANK_A, classification: SINGLE, count: count }
        situation = check_for_failed_situation(unknown_job)
        unknown_lines += ["#{convert_impact(unknown_job[:impact])}|#{unknown_job[:key]}|#{unknown_job[:count]}|#{situation}"]
        next
      end

      # when param "simple" is passed, than only WARNING and CRITICAL are displayed
      if !simple_format? or (simple_format? && [CRITICAL, WARNING].include?(situation))
        lines += ["#{convert_impact(job[:impact])}|#{job[:key]}|#{job[:count]}|#{situation}"]
      end
    end

    write_file('result_failed.txt', lines)
    write_file('result_unknown.txt', unknown_lines)
  end

  #######################################################
  #### Pending
  #######################################################
  def check_pending
    total = 0
    lines = []

    @queues.each do |queue|
      job = @jobs.detect{ |k| k[:key] == queue }
      next unless job
      job[:count] = Resque.size(queue)

      situation = check_for_pending_situation(job)

      # when param "simple" is passed, than only WARNING and CRITICAL is displayed
      if !simple_format? or (simple_format? && [CRITICAL, WARNING].include?(situation))
        lines += ["#{convert_impact(job[:impact])}|#{job[:key]}|#{job[:count]}|#{situation}"]
      end
    end

    write_file('result_pending.txt', lines)
  end

  #######################################################
  #### Workers
  #######################################################
  def check_workers
    count = 0
    lines = []
    medium_jobs = []
    critical_jobs = []

    @jobs.each do |job|
      medium_jobs   << job if job[:impact] == MEDIUM
      critical_jobs << job if job[:impact] == CRITICAL
    end

    medium_jobs.each do |job|
      unless @queues.include?(job[:key])
        lines += ["2|#{job[:key]}|#{WARNING}"]
        count += 1
      end
    end

    critical_jobs.each do |job|
      unless @queues.include?(job[:key])
        lines += ["3|#{job[:key]}|#{CRITICAL}"]
        count += 1
      end
    end
    write_file('result_workers.txt', lines)
  end

  def get_redis
    resque_redis_host     = ENV['RESQUE_REDIS_HOST'] || 'localhost'
    resque_redis_port     = ENV['RESQUE_REDIS_PORT'] || 6380
    resque_redis_password = ENV['RESQUE_REDIS_PASSWORD']

    raise 'RESQUE_REDIS_HOST is not set' unless resque_redis_host
    raise 'RESQUE_REDIS_PORT is not set' unless resque_redis_port

    resque_redis_options = { host: resque_redis_host, port: resque_redis_port }
    resque_redis_options[:password] = resque_redis_password unless resque_redis_password.nil?
    redis = Redis.new(resque_redis_options)

    redis.ping
    redis
  end

  def load_resque
    resque = Redis::Namespace.new('iba:jobs:resque', redis: get_redis)
    Resque.redis = resque
    @queues  = Resque.queues
    @workers = Resque.workers
    @failed_jobs = Resque::Failure.all(0,-1)
    @unclassified = []

    @jobs = [{ key: "aggregate_account_dispatch_issues"                      , impact:    LOW, rank: RANK_A, classification: SINGLE, count: 0 },
             { key: "aggregate_account_load_publisher_accounts"              , impact:    LOW, rank: RANK_A, classification: SINGLE, count: 0 },
             { key: "aggregate_account_load_publisher_accounts_remote"       , impact:    LOW, rank: RANK_A, classification: SINGLE, count: 0 },
             { key: "aggregate_account_process_issue"                        , impact:    LOW, rank: RANK_A, classification: SINGLE, count: 0 },
             { key: "aggregate_account_remove_issue"                         , impact:    LOW, rank: RANK_A, classification: SINGLE, count: 0 },
             { key: "cancel_payment.cancel_pending_credit_card_registration" , impact: MEDIUM, rank: RANK_C, classification:   MASS, count: 0 },
             { key: "cancel_payment.fetch_pending_credit_card_registrations" , impact: MEDIUM, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "catalogue.mark_magazine_issues_for_reindex"             , impact:    LOW, rank: RANK_E, classification: SINGLE, count: 0 },
             { key: "catalogue.recreate_covers.books"                        , impact:    LOW, rank: RANK_G, classification:   MASS, count: 0 },
             { key: "catalogue.recreate_covers.processing"                   , impact:    LOW, rank: RANK_G, classification:   MASS, count: 0 },
             { key: "club_iba_additional_product_job"                        , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "club_iba_cancel_job"                                    , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "club_iba_create_user_billing"                           , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "club_iba_fetch_failed_invoices_to_reprocess"            , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "club_iba_membership_reminder_job"                       , impact: 	  LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "club_iba_payment_failure_job"                           , impact:   HIGH, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "club_iba_payment_failure_notice_job"                    , impact:    LOW, rank: RANK_E, classification: SINGLE, count: 0 },
             { key: "club_iba_welcome_job"                                   , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "create_item_entitlement"                                , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "crp_create_customer"                                    , impact: MEDIUM, rank: RANK_F, classification: SINGLE, count: 0 },
             { key: "crp_create_product"                                     , impact: MEDIUM, rank: RANK_F, classification: SINGLE, count: 0 },
             { key: "fetch_recurrences"                                      , impact:   HIGH, rank: RANK_B, classification:   MASS, count: 0 },
             { key: "global_collect"                                         , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "global_collect.boleto_payment"                          , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "global_collect.boleto_report"                           , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "global_collect.download_report"                         , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "global_collect_payments.fetch_old_pending_boletos"      , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "global_collect_payments.verify_and_decide_boleto"       , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "iba_subscribers_code"                                   , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "mailer"                                                 , impact:   HIGH, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "mailers_social_link_account_job"                        , impact:    LOW, rank: RANK_E, classification: SINGLE, count: 0 },
             { key: "mailers_user_welcome_job"                               , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "membership_reminder.fetch_reminders"                    , impact:   HIGH, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "order"                                                  , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "payment_recurrence_web_hook"                            , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "post_purchase_job"                                      , impact:   HIGH, rank: RANK_F, classification: SINGLE, count: 0 },
             { key: "process_invoice_batch_job"                              , impact:   HIGH, rank: RANK_C, classification:   MASS, count: 0 },
             { key: "process_invoice_job"                                    , impact:   HIGH, rank: RANK_C, classification:   MASS, count: 0 },
             { key: "process_invoice_of_iba_clube_job"                       , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "process_recurrence"                                     , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "profiles_password_reset_mailer_job"                     , impact:   HIGH, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "profiles_social_sync_job"                               , impact:    LOW, rank: RANK_G, classification:   MASS, count: 0 },
             { key: "project_group_mappings"                                 , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "refund.refund_credit_card_registration"                 , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "refund.refund_pending_credit_card_registrations"        , impact: MEDIUM, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "reprocess_invoice_batch_job"                            , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "site_catalyst_export_manifest_x_ref"                    , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "site_catalyst_import_manifest_x_ref"                    , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "sitemap"                                                , impact: MEDIUM, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "subscriptions.assine_users.send_email_to_user_job"      , impact:    LOW, rank: RANK_C, classification: SINGLE, count: 0 },
             { key: "subscriptions.feed.subscribers"                         , impact:   HIGH, rank: RANK_C, classification:   MASS, count: 0 },
             { key: "subscriptions.product_group.import"                     , impact:   HIGH, rank: RANK_A, classification:   MASS, count: 0 },
             { key: "subscriptions.product_group.process"                    , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 },
             { key: "web_hook"                                               , impact:   HIGH, rank: RANK_B, classification: SINGLE, count: 0 }
	]
  end

  def simple_format?
    ARGV.include?("simple")
  end

  def check_for_failed_situation(job)
    situation = NORMAL

    case job[:rank]
      ##A - 1 to 5 = Warning | More than 5 = Critical
      when 'A'
        situation = WARNING  if job[:count].between?(1,10)
        situation = CRITICAL if job[:count] > 5

      ##B - 10 to 20 = Warning | More than 20 = Critical
      when 'B'
        situation = WARNING  if job[:count].between?(10,20)
        situation = CRITICAL if job[:count] > 20

      ##C - 20 to 50 = Warning | More than 50 = Critical
      when 'C'
        situation = WARNING  if job[:count].between?(20,50)
        situation = CRITICAL if job[:count] > 50

      ##D - 50 to 100 = Warning | More than 100 = Critical
      when 'D'
        situation = WARNING  if job[:count].between?(50,100)
        situation = CRITICAL if job[:count] > 100

      ##E - 100 to 200 = Warning | More than 200 = Critical
      when 'E'
        situation = WARNING  if job[:count].between?(100,200)
        situation = CRITICAL if job[:count] > 200

      ##F - 200 to 1000 = Warning | More than 1000 = Critical
      when 'F'
        situation = WARNING  if job[:count].between?(200,1000)
        situation = CRITICAL if job[:count] > 1000

      ##G - 1000 to 2000 = Warning | More than 2000 = Critical
      when 'G'
        situation = WARNING  if job[:count].between?(1000,2000)
        situation = CRITICAL if job[:count] > 2000
    end

    #only HIGH queue sends CRITICAL
    situation = WARNING if situation == CRITICAL and job[:impact] != HIGH
    return situation
  end

  def check_for_pending_situation(job)
    situation = NORMAL

    case job[:classification]
      ##I - 10 to 50 = Warning | More than 50 = Critical
      when 'SINGLE'
        situation = WARNING  if job[:count].between?(10,50)
        situation = CRITICAL if job[:count] > 50

      ##M - 50 to 5000 = Warning | More than 5000 = Critical
      when 'MASS'
        situation = WARNING  if job[:count].between?(50,5000)
        situation = CRITICAL if job[:count] > 5000
    end

    #only HIGH queue sends CRITICAL
    situation = WARNING if situation == CRITICAL and job[:impact] != HIGH
    return situation
  end

  def write_file(filename, lines)
    lines.sort!
    File.open(FILEPATH + filename, "w+") { |f| lines.each { |line| f.write("#{line}\n") }}
    puts "Total de registros: #{lines.count - 1} - Exported to #{filename}"
  end

  def convert_impact(impact)
    case impact
    when 'LOW'
      return 1
    when 'MEDIUM'
      return 2
    when 'HIGH'
      return 3
    end
  end
end

ResqueMonitoring.new
