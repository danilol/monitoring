# iba Monitoring

## Check the health of iba environment

This repository was created to check the status of all iba applications and integrations to send alerts and alarms.


### Resque Monitoring
Monitors the status of executing queues for all active jobs.

### DPS Monitoring
Monitors the status of Adobe DPS service.

### Health Check
Monitors the application status and external services.
